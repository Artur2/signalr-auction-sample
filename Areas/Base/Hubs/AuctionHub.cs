﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Auction.Repositories;
using Microsoft.AspNet.SignalR;

namespace Auction.Areas.Base.Hubs {

	public class AuctionHub : Hub {

		/// <summary>
		/// Обновление цены
		/// <example>
		/// Пример вызова: 
		/// На стороне клиента - <c>auctionHub.server.AddPrice(ptitle,price,uname,msg);</c>
		/// </example>
		/// </summary>
		/// <param name="productTitle">Название продукта</param>
		/// <param name="price">Цена</param>
		/// <param name="userName">Имя пользователя</param>
		/// <param name="message">Сообщение пользователя</param>
		public void AddPrice( string productTitle, float price, string userName, string message ) {

			var product = ProductStaticRepository.Get( productTitle );

			product.Price = price;

			ProductStaticRepository.Save( product );

			Clients.Group( productTitle ).UpdateState( userName, message, price );
		}

		/// <summary>
		/// Вступление в группу по имени продукта в данном случае
		/// </summary>
		/// <param name="groupName">В данном проекте это имя продукта</param>
		public void JoinGroup( string groupName ) {
			Groups.Add( Context.ConnectionId, groupName );
		}
	}
}