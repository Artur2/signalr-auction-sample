﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Auction.Areas.Base.Models {
	
	public class Product {

		[DisplayName("Название продукта")]
		public string Title { get; set; }

		[DisplayName("Цена")]
		public float Price { get; set; }
	}
}