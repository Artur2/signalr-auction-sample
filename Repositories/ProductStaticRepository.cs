﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Auction.Areas.Base.Models;

namespace Auction.Repositories {

	public static class ProductStaticRepository {

		private static readonly List<Product> products = null;

		static ProductStaticRepository() {

			products = new List<Product>() {
				new Product() {Price = 100, Title = "Picture"},
				new Product() {Price = 50, Title = "Ball"}
			};
		}

		public static void Save( Product product ) {

			if ( products.Exists( _ => _.Title == product.Title ) ) {

				products.Remove( products.Find( _ => _.Title == product.Title ) );
				products.Add( product );
			}
		}

		public static Product Get( string title ) {

			return products.Find( _ => _.Title.Equals( title, StringComparison.InvariantCultureIgnoreCase ) );
		}

		public static IEnumerable<Product> GetAll() {

			return products;
		}

		public static IEnumerable<Product> GetFiltered( Func<Product, bool> predicate ) {

			return products.Where( predicate );
		}

	}
}